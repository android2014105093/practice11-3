package com.example.vrlab.ex_gles;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by SSELAB on 2016-11-14.
 */

public class Obj {
    protected FloatBuffer vertexBuffer;
    protected FloatBuffer colorBuffer;
    protected ByteBuffer indexBuffer;
    protected int polygonMode;
    protected int vertexLength;
    protected boolean colorVertex;
    protected boolean indexVertex;

    public Obj() {
        polygonMode = GL10.GL_TRIANGLE_STRIP;
        vertexLength = 0;
        colorVertex = false;
        indexVertex = false;
    }

    public void draw(GL10 gl) {
        gl.glFrontFace(GL10.GL_CW);
        if (colorVertex) {
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
            gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
            if (indexVertex) {
                gl.glDrawElements(polygonMode, vertexLength,
                        GL10.GL_UNSIGNED_BYTE, indexBuffer);
            }
            else {
                gl.glDrawArrays(polygonMode, 0, vertexLength);
            }
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
        }
        else {
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            if (indexVertex) {
                gl.glDrawElements(polygonMode, vertexLength,
                        GL10.GL_UNSIGNED_BYTE, indexBuffer);
            }
            else {
                gl.glDrawArrays(polygonMode, 0, vertexLength);
            }
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }
    }
}